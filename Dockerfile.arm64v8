FROM alpine AS builder

# Download QEMU, see https://github.com/docker/hub-feedback/issues/1261
ENV QEMU_URL https://github.com/balena-io/qemu/releases/download/v4.0.0-balena/qemu-4.0.0-balena-aarch64.tar.gz
RUN apk add curl && curl -L ${QEMU_URL} | tar zxvf - -C . --strip-components 1

FROM lsiobase/alpine:arm64v8-3.12

# Add QEMU
COPY --from=builder qemu-aarch64-static /usr/bin

ARG BUILDDATE

LABEL \
	app.deemix.image.created="${BUILDDATE}" \
	app.deemix.image.url="https://gitlab.com/Bockiii/deemix-docker" \
    app.deemix.image.title="Docker image for Deemix" \
	app.deemix.image.description="Docker image for Deemix and the pyweb frontend" \
    maintainer="Bocki"

EXPOSE 9666

RUN \
 echo "**** install build packages ****" && \
 apk add --no-cache --virtual=build-dependencies \
    gcc \
    g++ \
    libffi-dev \
    python3-dev \
    git \
    curl \
    make && \
 echo "**** install packages ****" && \
 apk add --no-cache \
    py3-pip \
    python3 && \
 echo "**** setup directories ****" && \
 mkdir /deem && \
 chown abc:abc /deem && \
 rm -R /config && \
 ln -sf /deem/.config/deemix /config && \
 ln -sf /downloads /deem/deemix\ Music && \
 echo "**** clean up ****" && \
 rm -rf \
	/root/.cache \
	/tmp/*

COPY root/ /

EXPOSE 6595
VOLUME /downloads /config